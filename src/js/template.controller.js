const SCROLL_SPEEDS = {
  1: 2000,
  3: 1000,
  5: 500,
  7: 250,
};

function TemplateController(iwDataService, $timeout, $scope) {
  this.pagination = {
    currentCategoryIndex: 0,
    slideDuration: 1000,
  };

  this.getHeaderStyles = (page) => ({
    backgroundColor: page.style.colors.titleBgColor,
    color: page.style.colors.titleColor,
    "font-size": `${page.style.fonts.titleFontSize}px`,
  });

  this.getItemStyles = (page) => ({
    color: page.style.colors.itemColor,
    "font-size": `${page.style.fonts.rowFontSize}px`,
  });

  this.getCategoryStyles = (page) => ({
    background: `linear-gradient(${page.style.colors.backgroundOverlay},${page.style.colors.backgroundOverlay}),url(${page.style.images.background})`,
  });

  this.getItemContainerStyles = (row) => ({
    "column-count": this.columns,
  });

  iwDataService.get().then((server) => {
    Object.keys(server).forEach((key) => {
      this[key] = server[key];
    });

    const setListItemValue = (value) => {
      return value && value.length > 0 ? value : "Sample Text";
    };

    this._lastScrollSpeed = SCROLL_SPEEDS[this.settings.slide.scrollSpeed];

    this.postMessageCallback = async (doScopeUpdate = true) => {
      this.scrollSpeed = SCROLL_SPEEDS[this.settings.slide.scrollSpeed];
      this.columns = parseInt(this.settings.slide.columns);

      if (this.scrollSpeed !== this._lastScrollSpeed) {
        this._lastScrollSpeed = this.scrollSpeed;
      }

      resetAnimation(this.scrollSpeed);

      var singlePage = window.IWUtils.applyEffects(
        this.data,
        this.settings
      ).map((page) => {
        page.rows = page.items.reduce((memo, val, index) => {
          var rowIndex = Math.floor(index / this.columns);
          var rowCol = index % this.columns;
          memo[rowIndex] = memo[rowIndex] || { items: [], index };

          val.list_item = setListItemValue(val.list_item);

          memo[rowIndex].items[rowCol] = val;

          return memo;
        }, []);

        delete page.items;

        return page;
      });

      const getSinglePage = () => JSON.parse(JSON.stringify(singlePage));

      const createList = () =>
        [].concat(getSinglePage(), getSinglePage(), getSinglePage());

      this.pages = createList();

      const stickyHeader = angular.element(
        document.getElementById("sticky-header")
      );

      if (stickyHeader[0].style) {
        stickyHeader[0].style.backgroundColor = this.settings.page[0].style.colors.titleBgColor;
      }

      if (doScopeUpdate) {
        $scope.$apply();
      }
    };

    this.postMessageCallback(false);

    function resetAnimation(scrollSpeed) {
      const $container = angular.element(document.querySelector(".outer"));

      const $header = angular.element(document.getElementById("sticky-header"));

      const $list = angular.element(document.querySelector(".main-container"));

      stopAnimation($container, $header, $list);
      bootstrapAnimation(scrollSpeed);
    }

    function bootstrapAnimation(scrollSpeed) {
      const list = angular.element(document.querySelector(".main-container"));

      const stickyHeader = angular.element(
        document.getElementById("sticky-header")
      );

      const outerContainer = angular.element(document.querySelector(".outer"));

      // startAnimation lives in animations.js
      startAnimation(outerContainer, stickyHeader, list, scrollSpeed);
    }

    $timeout(() => {
      bootstrapAnimation(this.scrollSpeed);
    }, 0);
  });
}

const app = angular
  .module("app")
  .controller("TemplateController", [
    "iwDataService",
    "$timeout",
    "$scope",
    TemplateController,
  ]);
