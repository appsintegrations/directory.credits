const PPS = 59

const figureNextIterationLength = (height, speed) => height / (PPS / speed)

const stopAnimation = (container, header, list) => {
  window.Velocity(list, 'stop')
  window.Velocity(container, 'stop')

  // set the transform to 0 since the sticky-header no longer has a value
  container.css({ transform: 'translate(0, 0)' })

  // getting the newList back to translateY: 0
  list.css({ transform: 'translate(0, 0)' })

  header.empty()
}

const startAnimation = (container, header, list, scrollSpeed) => {
  const slideCurrentHeaderOutOfView = ({ headerHeight }) => {
    const duration = figureNextIterationLength(headerHeight, scrollSpeed)

    window.Velocity(
      container,
      {
        translateY: [-headerHeight, 0],
      },
      {
        duration,
        easing: 'linear',
        complete: () => prepareNextIteration(list, scrollSpeed),
      }
    )
  }

  const prepareNextIteration = (newList, scrollSpeed) => {
    // delete the contents of the stuck header
    header.empty()

    // set the transform to 0 since the sticky-header no longer has a value
    container.css({ transform: 'translate(0, 0)' })

    // getting the newList back to translateY: 0
    newList.css({ transform: 'translate(0, 0)' })

    // detaching and attaching the node again
    newList.append(angular.element(newList.children()[0]).detach())

    startAnimation(container, header, newList, scrollSpeed)
  }

  // theres no pages to paginate
  if (list.children().length === 0) {
    console.log('animation not running because there are no pages to animate')
    return
  }

  const elemHeight = list.children()[0].offsetHeight

  // Append the category's title to the #stickyHeader element to simulate a 'position:Sticky' element
  header.append(angular.element(angular.element(list.children()[0]).children()[0]).clone())

  const headerHeight = header.children()[0].offsetHeight

  // We need the height so we know how far we need to scroll
  const height = elemHeight - headerHeight <= 0 ? 0 : elemHeight - headerHeight

  // figure out the duration needed to complete the transition based on the scroll speed and total height
  const duration = figureNextIterationLength(height, scrollSpeed)

  window.Velocity(
    list,
    {
      translateY: [-height, 0],
      cache: false,
    },
    {
      cache: false,
      duration,
      easing: 'linear',
      complete: () => slideCurrentHeaderOutOfView({ headerHeight }),
    }
  )
}
